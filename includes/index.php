<?php
function ltc_rdr_index() { 
	function send_message($message){
		$node_url = get_option('p16_node_url');
		$app_token = get_option('p16_app_token');
		$message = urlencode($message);
		$url_rmt = "{$node_url}/send_telegram?token={$app_token}&message={$message}";
		$response = wp_remote_get($url_rmt);
		if (is_wp_error($response)) {
			return 'Error obteniendo datos: ' . $response->get_error_message();
		}
		$body = wp_remote_retrieve_body($response);
		echo $body."<br>";
	}
	function set_config($attr, $value){
		$node_url = get_option('p16_node_url');
		$app_token = get_option('p16_app_token');
		$value = urlencode($value);
		$url_rmt = "{$node_url}/set_config?token={$app_token}&attr={$attr}&value={$value}";
		$response = wp_remote_get($url_rmt);
		if (is_wp_error($response)) {
			return 'Error obteniendo datos: ' . $response->get_error_message();
		}
		$body = wp_remote_retrieve_body($response);
		echo $body."<br>";
	}
	if(isset($_REQUEST['accion'])){
		switch($_REQUEST['accion']){
			case 'send_ping':
				send_message("Mensaje de Prueba!");
				break;
			case 'set_config':
				if(isset($_POST['tg_token'])){
					$tg_token = $_POST['tg_token'];
					update_option('p16_token', $tg_token);
					set_config("tlg.token", $tg_token);
				}else if(isset($_POST['tg_users'])){
					$tg_users = $_POST['tg_users'];
					$tg_users = str_replace(' ', '', $tg_users);
					$tg_users_arr = explode(",",$tg_users);
					update_option('p16_users', json_encode($tg_users_arr));
					set_config("tlg.users", json_encode($tg_users_arr));
				}else if(isset($_POST['node_url'])){
					$node_url = $_POST['node_url'];
					update_option('p16_node_url', $node_url);
				}else if(isset($_POST['noti_format'])){
					$noti_format = $_POST['noti_format'];
					update_option('p16_noti_format', $noti_format);
				}else if(isset($_POST['app_token'])){
					$app_token = $_POST['app_token'];
					update_option('p16_app_token', $app_token);
				}
				echo "Cambios han sido efectuados";
				break;
		}
		// exit;
	}
	
	$app_token = get_option('p16_app_token');
	$node_url = get_option('p16_node_url');
	$tg_token = get_option('p16_token');
	$noti_format = get_option('p16_noti_format');
	if(get_option('p16_users')){
		$tg_users = json_decode(get_option('p16_users'));
		$tg_users_value = join(",", $tg_users);
	}else{
		$tg_users_value = "";
	}
	
	$plugin_base_path = plugin_dir_path( __FILE__ );
	include($plugin_base_path.'..\init.php');
?>
<div class="section">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<h3 class="mt-3 mb-3">TgNotificador</h3>
	<form method="post" action="#">
	<div class="form-group">
		<label for="exampleInputEmail1">Ingrese Token App</label>
		<input type="text" class="form-control" name="app_token" value="<?=$app_token?>" required>
		<small id="emailHelp" class="form-text text-muted">Deberá ingresar el token autorizado de su Plugin.</small>
	</div>
	<button type="submit" name="accion" value="set_config" class="btn btn-sm btn-warning">Establecer</button>
	</form>
	<form method="post" action="#">
	<div class="form-group">
		<label for="exampleInputEmail1">Ingrese URL server Node</label>
		<input type="text" class="form-control" name="node_url" value="<?=$node_url?>">
		<small class="form-text text-muted">Deberá ingresar el URL de NODEJS script</small>
	</div>
	<button type="submit" name="accion" value="set_config" class="btn btn-sm btn-warning">Establecer</button>
	</form>
	<form method="post" action="#">
	<div class="form-group">
		<label for="exampleInputEmail1">Ingrese Token Telegram bot</label>
		<input type="text" class="form-control" name="tg_token" value="<?=$tg_token?>" required>
		<small id="emailHelp" class="form-text text-muted">Deberá ingresar el token de su bot de telegram proporcionado por <a href="https://t.me/BotFather">@botfather</a>.</small>
	</div>
	<button type="submit" name="accion" value="set_config" class="btn btn-sm btn-warning">Establecer</button>
	</form>
	<form method="post" action="#">
		<div class="form-group">
			<label for="exampleInputPassword1">Ingrese ID de usuarios Telegram</label>
			<input type="text" class="form-control" name="tg_users" value="<?=$tg_users_value?>">
			<small id="emailHelp" class="form-text text-muted">Ingrese el ID de los usuarios a notificar separados por una coma. Puede obtener su ID de su cuenta telegram en <a href="https://t.me/userinfobot">@userinfobot</a></small>
		</div>
		<button type="submit" name="accion" value="set_config" class="btn btn-sm btn-warning">Establecer</button>
	</form>

	<form method="post" action="#">
		<div class="form-group">
			<textarea name="noti_format" class="form-control" rows="4"><?=$noti_format?></textarea>
			<small class="form-text text-muted">{{details}}: Detalle del pedido, {{total}}: Total de Compra, {{nro_pedido}}: Nro Pedido, {{link_pedido}}: Link del Pedido</small>
		</div>
		<button type="submit" name="accion" value="set_config" class="btn btn-sm btn-warning">Establecer</button>
	</form>
	<br>
	<form method="post" action="#">
		<button type="submit" name="accion" value="send_ping" class="btn btn-primary">Enviar mensaje de prueba</button>
	</form>
</div>

<?php } ?>