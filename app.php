<?php
/* 
Plugin Name: P16 Woocommerce Notificador con Telegram
Plugin URI: https://www.solsitecinnova.com/tienda
Description: Implementa un notificador tu tienda woocommerce con este plugin para recibir un mensaje por cada pedido realizado por tus clientes.
Version: 1.3
Author: Luis Torres
Author URI: https://www.solsitecinnova.com
License: GPLv2 
*/

$plug_prefix = "p16_";

function ltc_Activar(){
	// aqui inicializar la construccion de la base de datos
	global $wpdb;
	
	// $sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ltc_prueba (
	// 	  id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	// 	  nombres VARCHAR(50),
	// 	  apellidos VARCHAR(50),
	// 	  celular VARCHAR(15),
	// 	  correo VARCHAR(100)
	// 	)";
	// $wpdb->query($sql);
	update_option('p16_app_token', '5sZ3Ebwyx0iDtPG');
	update_option('p16_node_url', 'http://localhost:3000');
	update_option('p16_noti_format', "<b>Nuevo pedido #{{nro_pedido}}</b>
	Valorizado en {{total}}, contiene los siguientes detalles:
	{{details}} 
	
	Link de Pedido: {{link_pedido}}");
}
function ltc_Desactivar(){
	flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'ltc_Activar');
register_deactivation_hook(__FILE__, 'ltc_Desactivar');

add_action( "wp_ajax_do_{$plug_prefix}controllerAux", 'ltc_directory2' );
// add_action( 'wp_ajax_do_controllerAux', 'ltc_directory2' );

add_action('admin_menu', 'ltc_CrearMenu');

function ltc_CrearMenu(){
	$plug_prefix = $GLOBALS['plug_prefix'];
	add_menu_page(
		'TgNotificador', //page title
        'TgNotificador', //menu title
        'manage_options', //capabilities
        $plug_prefix.'index', //menu slug
        'ltc_rdr_index', //function
        null,
    	null
    );

    // Load Directory
    ltc_directory2();
}

function ltc_directory2(){
	if(isset($_REQUEST['controller'])){
		include('includes/'.$_REQUEST['controller'].'.php');
		eval("{$plug_prefix}rdr_".$_REQUEST['controller']."();");
	}else{
		// All Pages
		include('includes/index.php');
	}
}

add_action( 'woocommerce_thankyou', 'my_callback');

// my_callback();
function my_callback($order_get_id){
	$currentURL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	// echo $currentURL;
	if( strpos($currentURL, "onlyview") !== false  ){
		echo "Solo vista";
		return;
	}
	$order = wc_get_order($order_get_id);
	$total_orden = $order->get_total();
	// Inicializar un array para guardar la información de los productos
    $productos = array();
    // Iterar sobre los ítems del pedido
    foreach ($order->get_items() as $item_id => $item) {
        // Obtener el producto del ítem
        $product = $item->get_product();
		// print_r($item);
        // Verificar si el producto es válido
        if ($product) {
            $productos[] = array(
                'nombre' => $product->get_name(),
                'cantidad' => $item->get_quantity(),
                'subtotal' => $item->get_subtotal()
            );
        }
    }
	$node_url = get_option('p16_node_url');
	$app_token = get_option('p16_app_token');
	$noti_format = get_option('p16_noti_format');

	$details_html = "\n";
	foreach ($productos as $k => $pro) {
		$details_html.= "(".$pro['cantidad'].") " .$pro['nombre']." \n->subtotal de ".$pro['subtotal']." soles\n";
	}
	$noti_format = str_replace("{{total}}", "S/".$total_orden, $noti_format);
	
	$noti_format = str_replace("{{details}}", $details_html, $noti_format);

	$noti_format = str_replace("{{nro_pedido}}", $order_get_id, $noti_format);
	
	$link_onlyview = $currentURL."&onlyview=1";
	$tmp = "<a href='".$link_onlyview."'>Link de Pedido</a>";
	$noti_format = str_replace("{{link_pedido}}", $tmp, $noti_format);
	
	$message = urlencode($noti_format);
	// echo $message;
	// $message = urlencode("<b>Hicieron pedido</b> de {$productos[0]['cantidad']} unidad(es) del producto <b>\"{$productos[0]['nombre']}\"</b>");
	$url_rmt = "{$node_url}/send_telegram?token={$app_token}&message={$message}";
	$response = wp_remote_get($url_rmt);
	if (is_wp_error($response)) {
		return 'Error obteniendo datos: ' . $response->get_error_message();
	}
	// $body = wp_remote_retrieve_body($response);
	// echo $body;
}