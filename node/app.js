const fs = require('fs');
const path = require('path');
require('dotenv').config();

// var mod = "/node/demo";
var mod = "";

function updateEnvFile(key, value) {
    const envPath = path.join(__dirname, '.env');
    let envConfig = fs.readFileSync(envPath, 'utf-8');
    let hasKey = false;

    // Split the file into lines
    let lines = envConfig.split('\n');

    // Modify the existing line or note that the key doesn't exist
    lines = lines.map(line => {
        if (line.match(new RegExp(`^${key}=`))) {
            hasKey = true;
            return `${key}=${value}`; // Replace the line with the new value
        }
        return line;
    });

    // If the key wasn't found, add it to the end
    if (!hasKey) {
        lines.push(`${key}=${value}`);
    }

    // Join all lines back into a single string
    envConfig = lines.join('\n');

    // Write the new .env file contents
    fs.writeFileSync(envPath, envConfig, 'utf-8');
}

const TelegramBot = require('node-telegram-bot-api');
const express = require('express')
const app = express()

const port = process.env.PORT || 3000;
// replace the value below with the Telegram token you receive from @BotFather
var token = process.env['tlg.token'];
console.log("token: ", token)

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

// Listen for any kind of message. There are different kinds of
// messages.
var users_notificar = JSON.parse(process.env['tlg.users']);
console.log(users_notificar.length)
if( users_notificar.length == 0 ){
    console.log("No hay Usuarios de destino")
}
// var users_notificar = [741706788, 6394305040];
bot.on('message', (msg) => {
//   const chatId = msg.chat.id;
  // send a message to the chat acknowledging receipt of their message
//   bot.sendMessage(users_notificar[0], 'Received your message');
});

app.get(mod+'/', function (req, res) {
    res.send('Running '+__dirname)
})

app.get(mod+'/send_telegram', function (req, res) {
    var q = req.query;
    var message = decodeURI(q.message);
    console.log(message)
    
    if(process.env['app_token'] != q.token){
        res.send('no valido')
        return;
    }
    for( i in users_notificar ){
        bot.sendMessage(users_notificar[i], message, {parse_mode: 'HTML'});
    }
    res.send('Mensaje enviado')
})
app.get(mod+'/set_config', function(req, res){
    var token = req.query.token;
    var attr = req.query.attr;
    console.log(req.query);
    if(process.env['app_token'] != token){
        res.send('no valido')
        return;
    }
    var tlg = ['tlg.token','tlg.users'];
    if (tlg.includes(attr)) {
        var value = req.query.value;
        updateEnvFile(attr, value);
        console.log("Se actulizo parametros")
    }
    res.send('executed!')
    process.exit() // restart in forever execute
})
app.listen(port)

console.log("en ejecución: "+port)